drop table Points;

create table Points(
    p string,
    n string)
row format delimited fields terminated by ',' stored as textfile;

load data local inpath '${hiveconf:G}' overwrite into table Points;

from Points insert overwrite table Points select Points.p, count(1) where 1=1 group by Points.p;

from Points insert overwrite table Points select Points.n, count(1) as cnt where 1=1 group by Points.n;

select * from Points;
